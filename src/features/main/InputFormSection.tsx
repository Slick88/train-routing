import React from 'react';
import styles from './Main.module.css';
import { Button, Dropdown, DropdownButton } from 'react-bootstrap';
import data from './stations.json';

const getListOfDropdownStations = () => {
  return Object.keys(data).map((keyValue) => <Dropdown.Item eventKey={keyValue}>{keyValue}</Dropdown.Item>);
};

const InputFormSection = (
  handleOnSelect: (point: string, value: any) => void,
  onClickSubmit: () => void,
  start: string,
  end: string,
) => {
  return (
    <>
      <div className={styles.row}>
        <span className={styles.label}>Starting Point</span>

        <DropdownButton
          id="start-dropdown"
          title={start ? start : 'Starting Point'}
          onSelect={(value: string) => handleOnSelect('start', value)}
        >
          {getListOfDropdownStations()}
        </DropdownButton>

        {/*{console.log((Object.keys(data)))}*/}
      </div>

      <div className={styles.row}>
        <span className={styles.label}>Ending Point</span>
        <DropdownButton
          id="end-dropdown"
          title={end ? end : 'Ending Point'}
          onSelect={(value: string) => handleOnSelect('end', value)}
        >
          {getListOfDropdownStations()}
        </DropdownButton>
      </div>
      <div className={styles.row}>
        <Button onClick={onClickSubmit}>Go</Button>
      </div>
    </>
  );
};

export default InputFormSection;
