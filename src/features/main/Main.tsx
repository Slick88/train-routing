import React, { useEffect, useState } from 'react';
import { Button, Modal, Table } from 'react-bootstrap';
import styles from './Main.module.css';
import PageWrapper from '../../containers/PageWrapper';
import utils from './utils';
import InputFormSection from './InputFormSection';
import RouteModal from './RouteModal';
import RoutingTable from './RoutingRable';

export const Main = () => {
  const [start, setStart] = useState('Jurong East');
  const [end, setEnd] = useState('Bukit Batok');
  const [showRoute, toggleRoute] = useState(false);
  const [routes, setRoutes] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [detailRoute, setDetailRoute] = useState(new Set());

  const handleCloseModal = () => setShowModal(false);
  const handleShowModal = (route: Set<string>) => {
    setDetailRoute(route);
    setShowModal(true);
  };

  useEffect(() => {
    utils.getLines();
  }, []);

  const onClickSubmit = () => {
    // @ts-ignore
    setRoutes(utils.getPath(start, end));
    toggleRoute(!showRoute);
  };

  const handleOnSelect = (point: string, value: any) => {
    switch (point) {
      case 'start':
        setStart(value);
        break;
      default:
        setEnd(value);
    }
    toggleRoute(false);
  };

  return (
    <div>
      {InputFormSection(handleOnSelect, onClickSubmit, start, end)}
      {showRoute && RoutingTable(start, end, routes, handleShowModal)}
      {RouteModal(showModal, handleCloseModal, detailRoute)}
    </div>
  );
};

export default PageWrapper(Main);
