import { Button, Modal } from 'react-bootstrap';
import React from 'react';

const RouteModal = (showModal: boolean, handleCloseModal: () => void, detailRoute: Set<any>) => {
  const routeArray = Array.from(detailRoute);
  const resultArray = [`Start from ${routeArray.shift()}`];

  let lastStation = '';
  routeArray.forEach((element, index) => {
    if (/^Take/.test(element)) {
      // start
      resultArray.push(`${element}`);
    } else if (/^Change/.test(element)) {
      // replace the previous take or change statement
      const lastItem: string = resultArray.pop() as string;
      let sliceAt = 0;
      if (/^Take/.test(lastItem)) {
        sliceAt = lastItem.search('towards') + 7;
      } else {
        sliceAt = lastItem.search('towards') + 7;
      }
      resultArray.push(`${lastItem.slice(0, sliceAt)} ${lastStation}`);
      resultArray.push(element);
    } else {
      lastStation = element;
      if (index === routeArray.length - 1) {
        const lastItem: string = resultArray.pop() as string;
        let sliceAt = 0;
        if (/^Take/.test(lastItem)) {
          sliceAt = lastItem.search('towards') + 7;
        } else {
          sliceAt = lastItem.search('towards') + 7;
        }
        resultArray.push(`${lastItem.slice(0, sliceAt)} ${lastStation}`);
        resultArray.push(`End at ${lastStation}`);
      }
    }
  });

  return (
    <Modal show={showModal} onHide={handleCloseModal}>
      <Modal.Header closeButton>
        <Modal.Title>Route Details</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {resultArray.map((e) => (
          <li>{e}</li>
        ))}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleCloseModal}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default RouteModal;
