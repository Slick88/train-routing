import React from 'react';
import utils from './utils';

describe('Utils test', () => {
  it('should get correct path way', function () {
    utils.getLines();
    expect(utils.getPath('Bukit Batok', 'Jurong East')).toMatchSnapshot();
  });

  it('should get path way overview', function () {
    utils.getLines();
    expect(utils.getOverview(utils.getPath('Bukit Batok', 'Jurong East')[0])).toMatchSnapshot();
  });
});
