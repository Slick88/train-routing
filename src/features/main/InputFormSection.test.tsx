import React from 'react';
import { mount, ReactWrapper } from 'enzyme';
import InputFormSection from './InputFormSection';

let mainWrapper: ReactWrapper<any, Readonly<{}>, React.Component<{}, {}, any>>;
const mockHandleOnSelect = jest.fn();
const mockOnClickSubmit = jest.fn();

const render = (props: any) =>
  mount(
    // @ts-ignore
    <InputFormSection {...props} />,
  );

describe('Main page', () => {
  beforeEach(() => {
    mainWrapper = render({
      handleOnSelect: mockHandleOnSelect,
      onClickSubmit: mockOnClickSubmit,
      start: 'Jurong East',
      end: 'Bukit Batok',
    });
  });
  it('should render correctly', function () {
    expect(mainWrapper).toMatchSnapshot();
  });
});
