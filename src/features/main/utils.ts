import file from './stations.json';

interface stationData {
  name: string;
  data: any;
  neighbours: Set<string>;
  addNeighbours(trainMap: fileStationInterface): void;
}
interface fileStationInterface {
  [name: string]: any;
}

const map = new Map();
const mapOrderedByLine = new Map();

const final: Set<string>[] = [];

const makeStation = (name: string, data: object): stationData => {
  return {
    name: name,
    data: data,
    neighbours: new Set(),
    addNeighbours() {
      // each line will have 1 or 2 neighbours
      for (let [linename, number] of Object.entries(this.data)) {
        // iterate thru lines
        const lineMap = mapOrderedByLine.get(linename);

        if (typeof number === 'number') {
          let frontNeighbour = number + 1,
            backNeighbour = number - 1;
          let frontFound = false,
            backFound = false;
          do {
            if (frontNeighbour <= lineMap.size) {
              if (lineMap.has(frontNeighbour)) {
                this.neighbours.add(lineMap.get(frontNeighbour).name);
                frontFound = true;
              } else frontNeighbour++;
            } else frontFound = true;
          } while (!frontFound);

          do {
            if (backNeighbour > 0) {
              if (lineMap.has(backNeighbour)) {
                this.neighbours.add(lineMap.get(backNeighbour).name);
                backFound = true;
              } else backNeighbour--;
            } else backFound = true;
          } while (!backFound);
        }
        if (typeof number === 'object') {
          // @ts-ignore
          for (let indivNumber of number) {
            let frontNeighbour = indivNumber + 1,
              backNeighbour = indivNumber - 1;
            let frontFound = false,
              backFound = false;
            do {
              if (frontNeighbour <= lineMap.size) {
                if (lineMap.has(frontNeighbour)) {
                  this.neighbours.add(lineMap.get(frontNeighbour).name);
                  frontFound = true;
                } else frontNeighbour++;
              } else frontFound = true;
            } while (!frontFound);

            do {
              if (backNeighbour > 0) {
                if (lineMap.has(backNeighbour)) {
                  this.neighbours.add(lineMap.get(backNeighbour).name);
                  backFound = true;
                } else backNeighbour--;
              } else backFound = true;
            } while (!backFound);
          }
        }
      }
    },
  };
};

const compareRoutes = (a: Set<string>, b: Set<string>) => {
  if (a.size < b.size) {
    return -1;
  }
  if (a.size > b.size) {
    return 1;
  }
  return 0;
};

const getLines = () => {
  for (const [key, value] of Object.entries(file)) {
    map.set(key, makeStation(key, value));
    getStation(key, value);
  }
  for (const value of map.values()) {
    value.addNeighbours();
  }
};

const getStation = (name: string, data: any) => {
  for (let [key, value] of Object.entries(data)) {
    // check if line exist and create if doesnt exist
    // add to line the station
    if (mapOrderedByLine.has(key)) {
      const trainLine = mapOrderedByLine.get(key);
      trainLine.set(value, makeStation(name, data));
    } else {
      mapOrderedByLine.set(key, new Map([[value, makeStation(name, data)]]));
    }
  }
};

function travelAlgorithm(travelSet: Set<string>, currentLine: string, current: string, end: string) {
  if (current === end) {
    // console.log('End reached ',travelSet,' ', new Set(final))
    final.push(travelSet);
  } else {
    // console.log('Travelling ',travelSet,' ',currentLine, ' ', new Set(final))

    const currentData = map.get(current);
    let neighbours = currentData.neighbours;
    // @ts-ignore
    neighbours = Array.from(neighbours).filter((element: string) => !travelSet.has(element));
    if (neighbours.length > 0 && travelSet.size <= 29) {
      neighbours.forEach((neighbour: string) => {
        const nextSet = new Set(travelSet);
        if (currentLine === '') {
          // for first to second station
          const currentLines = Object.keys(map.get(current).data);
          const nextLines = new Set(Object.keys(map.get(neighbour).data));
          const r = currentLines.filter((elements) => nextLines.has(elements));
          nextSet.add(`Take from ${current} ${r[0]} line towards ` + neighbour);
          travelAlgorithm(nextSet.add(neighbour), r[0], neighbour, end);
        } else {
          // rest of moves
          const nextLinesArray = Object.keys(map.get(neighbour).data);
          const nextLines = new Set(nextLinesArray);
          // both station are on the same line
          if (nextLines.has(currentLine)) {
            travelAlgorithm(nextSet.add(neighbour), currentLine, neighbour, end);
          } else {
            // check for common line
            const currentLines = Object.keys(map.get(current).data);
            const r = currentLines.filter((elements) => nextLines.has(elements));
            nextSet.add(`Change at ${current} to ${r[0]} line towards ` + neighbour);
            travelAlgorithm(nextSet.add(neighbour), r[0], neighbour, end);
          }
        }
      });
    }
  }
}

const getPath = (start: string, end: string) => {
  const startSet = new Set([start]);

  // call loop
  travelAlgorithm(startSet, '', start, end);

  final.sort((a, b) => compareRoutes(a, b));
  // Get top 5 routes based on travel distance
  return final.slice(0, 5);
};

const getOverview = (route: Set<string>) => {
  const routeArray = Array.from(route);
  console.log(routeArray);

  return routeArray.filter((station) => map.has(station));
};
export default {
  getLines,
  getPath,
  getOverview,
};
