import utils from './utils';
import { Table } from 'react-bootstrap';
import React from 'react';

const renderOverview = (route: Set<string>) => {
  const routeOverview = utils.getOverview(route);
  return (
    <>{routeOverview.reduce((accumulator: string, currentValue: string) => accumulator + ' -> ' + currentValue)}</>
  );
};

const renderRouteDetails = (routes: Set<string>[], handleShowModal: (arg0: Set<string>) => void) => {
  return routes.map((route, index) => {
    const { size } = route;
    return (
      <tr onClick={() => handleShowModal(route)}>
        <td>{index + 1}</td>
        <td>{renderOverview(route)}</td>
        <td>{size - 1}</td>
      </tr>
    );
  });
};

const RoutingTable = (start: string, end: string, routes: any[], handleShowModal: (route: Set<string>) => void) => {
  return (
    <div>
      From {start} to {end}
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Route#</th>
            <th>Overview</th>
            <th>Estimated time</th>
          </tr>
        </thead>
        <tbody>{renderRouteDetails(routes, handleShowModal)}</tbody>
      </Table>
    </div>
  );
};

export default RoutingTable;
