READ ME
#Instructions

- Navigate to root directory of project in terminal
- run `npm install` to complete installation of npm packages
- run `npm start` to launch node server
- Navigate to `localhost:3000` on the web browser to access Routing page

## assumptions made

- it shouldnt take more that 30 stops to reach your destination
- each change of station counts as 1 additional actions
- Return top 5 routes based on length

## possible improvements?

- Add a searchable box for dropdown
- get distance or time between stations?
- possibly get timing to change train lines
- In an ideal situation, I would use redux
